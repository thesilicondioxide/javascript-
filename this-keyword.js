// var car = {
//     make: 'bmw',
//     model: '745li',
//     year: 2010,
//     getPrice:function(){
//         //perform dome calc
//         return 5000;
//     },
//     printDescription: function(){
//         console.log(this.make + '' + this.model);
//     }
// }

// car.printDescription();

// function first(){
//     return this;
// }

// console.log(first() === global);

// function second(){
//     'use strict';

//     return this;
// }

// console.log(second() === global);
// console.log(second() === undefined);

// let myObject = { value: "my Object"};

// //value is set it on the global object
// global.value= 'Global object';

// function third(){
//     return this.value ;
// }

// console.log(third());
// console.log(third.call(myObject, 'bob'));
// console.log(third.apply(myObject, ['bob']));

function fifth(){
    console.log(this.firstName + ' ' +this.lastName);
}

let customer1 = {
    firstName : 'silica',
    lastName : 'koirala',
    print: fifth
}

let customer2 = {
    firstName : 'Aalu',
    lastName : 'Potato',
    print: fifth
}

customer1.print();
customer2.print();