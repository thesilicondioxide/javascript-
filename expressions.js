//type of expressions
// a;

//type if expressions
//***************************************

//1.Variable declarations
// let a;
// a;

//2.Assign a value
// let a;

//3. perform an evaluation that returns single value
// b+c

let b = 3;
let c = 2;
//there are 3 expressions in here
let a = b + c;

//1. let a.... variable declaration
//2. perform an eval b+c
//3. result assigned to a